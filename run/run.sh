#!/bin/sh
if [[ "$1" = "" ]]; then
  TITLE=$(date '+%Y%m%d%H%M')
else
  TITLE="$1"
fi
#INPUTFILE="/gpfs/fs7001/ktaniguc/outputfile/calceffoutput_NSWold_test.root"
INPUTFILE="/gpfs/fs7001/ktaniguc/outputfile/calceffoutput_NSW_v2.root"
OUTPUTFILE="NSW"
BEGIN_ENTRY=0
LIMIT_ENTRY=-1
TRIG_CHAIN=0

mkdir -p ./output/${TITLE}
mkdir -p ./plot/${TITLE}
$setupDir/build/source/test/NtupleAnalysis.out $TITLE $INPUTFILE $OUTPUTFILE $BEGIN_ENTRY $LIMIT_ENTRY $TRIG_CHAIN
