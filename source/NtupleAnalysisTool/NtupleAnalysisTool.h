//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 10 12:48:54 2020 by ROOT version 6.12/04
// from TTree t_tap/TrigMuonTagAndProbe
// found on file: /gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/topoRoad_on_calcEffoutput.root
//////////////////////////////////////////////////////////

#ifndef NtupleAnalysisTool_h
#define NtupleAnalysisTool_h

#include <iostream>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TString.h"
#include "../NtupleAnalysisTool/HistData.h"
#include "/home/ktaniguc/RootUtils/RootUtils/TLegend_addfunc.h"
#include "/home/ktaniguc/RootUtils/RootUtils/TCanvas_opt.h"
#include "/home/ktaniguc/RootUtils/src/rootlogon.C"

// Header file for the classes stored in the TTree if any.
#include "vector"
using namespace std;

class NtupleAnalysisTool {
public :
   int m_trigchain{0};
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           eventNumber;
   Int_t           runNumber;
   Int_t           lumiBlock;
   Float_t         averageInteractionsPerCrossing;
   Double_t        tpsumReqdRl1;
   Double_t        tpextdR;
   Double_t        invMass;
   vector<string>  *trigname;
   Int_t           n_trig;
   Double_t        tag_pt;
   Double_t        tag_eta;
   Double_t        tag_phi;
   Double_t        tag_extEta;
   Double_t        tag_extPhi;
   Double_t        tag_d0;
   Double_t        tag_z0;
   Double_t        probe_pt;
   Double_t        probe_eta;
   Double_t        probe_phi;
   Double_t        probe_extEta;
   Double_t        probe_extPhi;
   Double_t        probe_d0;
   Double_t        probe_z0;
   Double_t        probe_segment_n;
   Double_t        probe_segment_x[10];
   Double_t        probe_segment_y[10];
   Double_t        probe_segment_z[10];
   Double_t        probe_segment_px[10];
   Double_t        probe_segment_py[10];
   Double_t        probe_segment_pz[10];
   Double_t        probe_segment_chiSquared[10];
   Double_t        probe_segment_numberDoF[10];
   Double_t        probe_segment_sector[10];
   Double_t        probe_segment_chamberIndex[10];
   Double_t        probe_segment_etaIndex[10];
   Double_t        probe_segment_nPrecisionHits[10];
   Double_t        probe_segment_nPhiLayers[10];
   Double_t        probe_segment_nTrigEtaLayers[10];
   vector<int>    *probe_L1_pass;
   vector<double>  *probe_L1_eta;
   vector<double>  *probe_L1_phi;
   vector<double>  *probe_L1_dR;
   vector<double>  *probe_L1_thrValue;
   vector<int>     *probe_L1_roiNum;
   vector<int>     *probe_L1_thrNumber;
   vector<int>    *probe_SA_pass;
   vector<double>  *probe_SA_pt;
   vector<double>  *probe_SA_eta;
   vector<double>  *probe_SA_phi;
   vector<double>  *probe_SA_etaBE;
   vector<double>  *probe_SA_phiBE;
   vector<double>  *probe_SA_etaMS;
   vector<double>  *probe_SA_phiMS;
   vector<double>  *probe_SA_tgcPt;
   vector<double>  *probe_SA_ptBarrelRadius;
   vector<double>  *probe_SA_ptBarrelSagitta;
   vector<double>  *probe_SA_ptEndcapAlpha;
   vector<double>  *probe_SA_ptEndcapBeta;
   vector<double>  *probe_SA_ptEndcapRadius;
   vector<double>  *probe_SA_ptCSC;
   vector<int>     *probe_SA_sAddress;
   vector<float>   *probe_SA_roiEta;
   vector<float>   *probe_SA_roiPhi;
   vector<int>     *probe_SA_isRpcFailure;
   vector<int>     *probe_SA_isTgcFailure;
   vector<double>  *probe_SA_superPointR_BI;
   vector<double>  *probe_SA_superPointR_BM;
   vector<double>  *probe_SA_superPointR_BO;
   vector<double>  *probe_SA_superPointR_EI;
   vector<double>  *probe_SA_superPointR_EM;
   vector<double>  *probe_SA_superPointR_EO;
   vector<double>  *probe_SA_superPointR_EE;
   vector<double>  *probe_SA_superPointR_CSC;
   vector<double>  *probe_SA_superPointR_BEE;
   vector<double>  *probe_SA_superPointR_BME;
   vector<double>  *probe_SA_superPointZ_BI;
   vector<double>  *probe_SA_superPointZ_BM;
   vector<double>  *probe_SA_superPointZ_BO;
   vector<double>  *probe_SA_superPointZ_EI;
   vector<double>  *probe_SA_superPointZ_EM;
   vector<double>  *probe_SA_superPointZ_EO;
   vector<double>  *probe_SA_superPointZ_EE;
   vector<double>  *probe_SA_superPointZ_CSC;
   vector<double>  *probe_SA_superPointZ_BEE;
   vector<double>  *probe_SA_superPointZ_BME;
   vector<double>  *probe_SA_superPointSlope_BI;
   vector<double>  *probe_SA_superPointSlope_BM;
   vector<double>  *probe_SA_superPointSlope_BO;
   vector<double>  *probe_SA_superPointSlope_EI;
   vector<double>  *probe_SA_superPointSlope_EM;
   vector<double>  *probe_SA_superPointSlope_EO;
   vector<double>  *probe_SA_superPointSlope_EE;
   vector<double>  *probe_SA_superPointSlope_CSC;
   vector<double>  *probe_SA_superPointSlope_BEE;
   vector<double>  *probe_SA_superPointSlope_BME;
   vector<double>  *probe_SA_superPointIntercept_BI;
   vector<double>  *probe_SA_superPointIntercept_BM;
   vector<double>  *probe_SA_superPointIntercept_BO;
   vector<double>  *probe_SA_superPointIntercept_EI;
   vector<double>  *probe_SA_superPointIntercept_EM;
   vector<double>  *probe_SA_superPointIntercept_EO;
   vector<double>  *probe_SA_superPointIntercept_EE;
   vector<double>  *probe_SA_superPointIntercept_CSC;
   vector<double>  *probe_SA_superPointIntercept_BEE;
   vector<double>  *probe_SA_superPointIntercept_BME;
   vector<double>  *probe_SA_superPointChi2_BI;
   vector<double>  *probe_SA_superPointChi2_BM;
   vector<double>  *probe_SA_superPointChi2_BO;
   vector<double>  *probe_SA_superPointChi2_EI;
   vector<double>  *probe_SA_superPointChi2_EM;
   vector<double>  *probe_SA_superPointChi2_EO;
   vector<double>  *probe_SA_superPointChi2_EE;
   vector<double>  *probe_SA_superPointChi2_CSC;
   vector<double>  *probe_SA_superPointChi2_BEE;
   vector<double>  *probe_SA_superPointChi2_BME;
   vector<vector<float> > *probe_SA_rpcHitX;
   vector<vector<float> > *probe_SA_rpcHitY;
   vector<vector<float> > *probe_SA_rpcHitZ;
   vector<vector<float> > *probe_SA_rpcHitR;
   vector<vector<float> > *probe_SA_rpcHitEta;
   vector<vector<float> > *probe_SA_rpcHitPhi;
   vector<vector<unsigned int> > *probe_SA_rpcHitMeasPhi;
   vector<vector<unsigned int> > *probe_SA_rpcHitLayer;
   vector<vector<string> > *probe_SA_rpcHitStationName;
   vector<vector<float> > *probe_SA_tgcHitZ;
   vector<vector<float> > *probe_SA_tgcHitR;
   vector<vector<float> > *probe_SA_tgcHitEta;
   vector<vector<float> > *probe_SA_tgcHitPhi;
   vector<vector<float> > *probe_SA_tgcHitWidth;
   vector<vector<int> > *probe_SA_tgcHitStationNum;
   vector<vector<bool> > *probe_SA_tgcHitIsStrip;
   vector<vector<int> > *probe_SA_tgcHitBCTag;
   vector<vector<bool> > *probe_SA_tgcHitInRoad;
   vector<vector<int> > *probe_SA_mdtHitIsOutlier;
   vector<vector<int> > *probe_SA_mdtHitChamber;
   vector<vector<float> > *probe_SA_mdtHitR;
   vector<vector<float> > *probe_SA_mdtHitZ;
   vector<vector<float> > *probe_SA_mdtHitPhi;
   vector<vector<float> > *probe_SA_mdtHitResidual;
   vector<vector<float> > *probe_SA_roadAw;
   vector<vector<float> > *probe_SA_roadBw;
   vector<float>  *probe_SA_tgcInnPhi;
   vector<vector<float> > *probe_SA_zMin;
   vector<vector<float> > *probe_SA_zMax;
   vector<vector<float> > *probe_SA_rMin;
   vector<vector<float> > *probe_SA_rMax;
   vector<vector<float> > *probe_SA_etaMin;
   vector<vector<float> > *probe_SA_etaMax;
   vector<vector<float> > *probe_SA_stgcClusterZ;
   vector<vector<float> > *probe_SA_stgcClusterR;
   vector<vector<float> > *probe_SA_stgcClusterEta;
   vector<vector<float> > *probe_SA_stgcClusterPhi;
   vector<vector<float> > *probe_SA_stgcClusterResidualR;
   vector<vector<float> > *probe_SA_stgcClusterResidualPhi;
   vector<vector<int> > *probe_SA_stgcClusterStationEta;
   vector<vector<int> > *probe_SA_stgcClusterStationPhi;
   vector<vector<int> > *probe_SA_stgcClusterStationName;
   vector<vector<int> > *probe_SA_stgcClusterType;
   vector<vector<int> > *probe_SA_stgcClusterIsOutlier;
   vector<vector<unsigned int> > *probe_SA_stgcClusterLayer;
   vector<vector<float> > *probe_SA_mmClusterZ;
   vector<vector<float> > *probe_SA_mmClusterR;
   vector<vector<float> > *probe_SA_mmClusterEta;
   vector<vector<float> > *probe_SA_mmClusterPhi;
   vector<vector<float> > *probe_SA_mmClusterResidualR;
   vector<vector<float> > *probe_SA_mmClusterResidualPhi;
   vector<vector<int> > *probe_SA_mmClusterStationEta;
   vector<vector<int> > *probe_SA_mmClusterStationPhi;
   vector<vector<int> > *probe_SA_mmClusterStationName;
   vector<vector<int> > *probe_SA_mmClusterIsOutlier;
   vector<vector<unsigned int> > *probe_SA_mmClusterLayer;
   vector<int>    *probe_CB_pass;
   vector<double>  *probe_CB_pt;
   vector<double>  *probe_CB_eta;
   vector<double>  *probe_CB_phi;
   vector<int>    *probe_EF_pass;
   vector<double>  *probe_EF_pt;
   vector<double>  *probe_EF_eta;
   vector<double>  *probe_EF_phi;

   // List of branches
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_tpsumReqdRl1;   //!
   TBranch        *b_tpextdR;   //!
   TBranch        *b_invMass;   //!
   TBranch        *b_trigname;   //!
   TBranch        *b_n_trig;   //!
   TBranch        *b_tag_pt;   //!
   TBranch        *b_tag_eta;   //!
   TBranch        *b_tag_phi;   //!
   TBranch        *b_tag_extEta;   //!
   TBranch        *b_tag_extPhi;   //!
   TBranch        *b_tag_d0;   //!
   TBranch        *b_tag_z0;   //!
   TBranch        *b_probe_pt;   //!
   TBranch        *b_probe_eta;   //!
   TBranch        *b_probe_phi;   //!
   TBranch        *b_probe_extEta;   //!
   TBranch        *b_probe_extPhi;   //!
   TBranch        *b_probe_d0;   //!
   TBranch        *b_probe_z0;   //!
   TBranch        *b_probe_segment_n;   //!
   TBranch        *b_probe_segment_x;   //!
   TBranch        *b_probe_segment_y;   //!
   TBranch        *b_probe_segment_z;   //!
   TBranch        *b_probe_segment_px;   //!
   TBranch        *b_probe_segment_py;   //!
   TBranch        *b_probe_segment_pz;   //!
   TBranch        *b_probe_segment_chiSquared;   //!
   TBranch        *b_probe_segment_numberDoF;   //!
   TBranch        *b_probe_segment_sector;   //!
   TBranch        *b_probe_segment_chamberIndex;   //!
   TBranch        *b_probe_segment_etaIndex;   //!
   TBranch        *b_probe_segment_nPrecisionHits;   //!
   TBranch        *b_probe_segment_nPhiLayers;   //!
   TBranch        *b_probe_segment_nTrigEtaLayers;   //!
   TBranch        *b_probe_L1_pass;   //!
   TBranch        *b_probe_L1_eta;   //!
   TBranch        *b_probe_L1_phi;   //!
   TBranch        *b_probe_L1_dR;   //!
   TBranch        *b_probe_L1_thrValue;   //!
   TBranch        *b_probe_L1_roiNum;   //!
   TBranch        *b_probe_L1_thrNumber;   //!
   TBranch        *b_probe_SA_pass;   //!
   TBranch        *b_probe_SA_pt;   //!
   TBranch        *b_probe_SA_eta;   //!
   TBranch        *b_probe_SA_phi;   //!
   TBranch        *b_probe_SA_etaBE;   //!
   TBranch        *b_probe_SA_phiBE;   //!
   TBranch        *b_probe_SA_etaMS;   //!
   TBranch        *b_probe_SA_phiMS;   //!
   TBranch        *b_probe_SA_tgcPt;   //!
   TBranch        *b_probe_SA_ptBarrelRadius;   //!
   TBranch        *b_probe_SA_ptBarrelSagitta;   //!
   TBranch        *b_probe_SA_ptEndcapAlpha;   //!
   TBranch        *b_probe_SA_ptEndcapBeta;   //!
   TBranch        *b_probe_SA_ptEndcapRadius;   //!
   TBranch        *b_probe_SA_ptCSC;   //!
   TBranch        *b_probe_SA_sAddress;   //!
   TBranch        *b_probe_SA_roiEta;   //!
   TBranch        *b_probe_SA_roiPhi;   //!
   TBranch        *b_probe_SA_isRpcFailure;   //!
   TBranch        *b_probe_SA_isTgcFailure;   //!
   TBranch        *b_probe_SA_superPointR_BI;   //!
   TBranch        *b_probe_SA_superPointR_BM;   //!
   TBranch        *b_probe_SA_superPointR_BO;   //!
   TBranch        *b_probe_SA_superPointR_EI;   //!
   TBranch        *b_probe_SA_superPointR_EM;   //!
   TBranch        *b_probe_SA_superPointR_EO;   //!
   TBranch        *b_probe_SA_superPointR_EE;   //!
   TBranch        *b_probe_SA_superPointR_CSC;   //!
   TBranch        *b_probe_SA_superPointR_BEE;   //!
   TBranch        *b_probe_SA_superPointR_BME;   //!
   TBranch        *b_probe_SA_superPointZ_BI;   //!
   TBranch        *b_probe_SA_superPointZ_BM;   //!
   TBranch        *b_probe_SA_superPointZ_BO;   //!
   TBranch        *b_probe_SA_superPointZ_EI;   //!
   TBranch        *b_probe_SA_superPointZ_EM;   //!
   TBranch        *b_probe_SA_superPointZ_EO;   //!
   TBranch        *b_probe_SA_superPointZ_EE;   //!
   TBranch        *b_probe_SA_superPointZ_CSC;   //!
   TBranch        *b_probe_SA_superPointZ_BEE;   //!
   TBranch        *b_probe_SA_superPointZ_BME;   //!
   TBranch        *b_probe_SA_superPointSlope_BI;   //!
   TBranch        *b_probe_SA_superPointSlope_BM;   //!
   TBranch        *b_probe_SA_superPointSlope_BO;   //!
   TBranch        *b_probe_SA_superPointSlope_EI;   //!
   TBranch        *b_probe_SA_superPointSlope_EM;   //!
   TBranch        *b_probe_SA_superPointSlope_EO;   //!
   TBranch        *b_probe_SA_superPointSlope_EE;   //!
   TBranch        *b_probe_SA_superPointSlope_CSC;   //!
   TBranch        *b_probe_SA_superPointSlope_BEE;   //!
   TBranch        *b_probe_SA_superPointSlope_BME;   //!
   TBranch        *b_probe_SA_superPointIntercept_BI;   //!
   TBranch        *b_probe_SA_superPointIntercept_BM;   //!
   TBranch        *b_probe_SA_superPointIntercept_BO;   //!
   TBranch        *b_probe_SA_superPointIntercept_EI;   //!
   TBranch        *b_probe_SA_superPointIntercept_EM;   //!
   TBranch        *b_probe_SA_superPointIntercept_EO;   //!
   TBranch        *b_probe_SA_superPointIntercept_EE;   //!
   TBranch        *b_probe_SA_superPointIntercept_CSC;   //!
   TBranch        *b_probe_SA_superPointIntercept_BEE;   //!
   TBranch        *b_probe_SA_superPointIntercept_BME;   //!
   TBranch        *b_probe_SA_superPointChi2_BI;   //!
   TBranch        *b_probe_SA_superPointChi2_BM;   //!
   TBranch        *b_probe_SA_superPointChi2_BO;   //!
   TBranch        *b_probe_SA_superPointChi2_EI;   //!
   TBranch        *b_probe_SA_superPointChi2_EM;   //!
   TBranch        *b_probe_SA_superPointChi2_EO;   //!
   TBranch        *b_probe_SA_superPointChi2_EE;   //!
   TBranch        *b_probe_SA_superPointChi2_CSC;   //!
   TBranch        *b_probe_SA_superPointChi2_BEE;   //!
   TBranch        *b_probe_SA_superPointChi2_BME;   //!
   TBranch        *b_probe_SA_rpcHitX;   //!
   TBranch        *b_probe_SA_rpcHitY;   //!
   TBranch        *b_probe_SA_rpcHitZ;   //!
   TBranch        *b_probe_SA_rpcHitR;   //!
   TBranch        *b_probe_SA_rpcHitEta;   //!
   TBranch        *b_probe_SA_rpcHitPhi;   //!
   TBranch        *b_probe_SA_rpcHitMeasPhi;   //!
   TBranch        *b_probe_SA_rpcHitLayer;   //!
   TBranch        *b_probe_SA_rpcHitStationName;   //!
   TBranch        *b_probe_SA_tgcHitZ;   //!
   TBranch        *b_probe_SA_tgcHitR;   //!
   TBranch        *b_probe_SA_tgcHitEta;   //!
   TBranch        *b_probe_SA_tgcHitPhi;   //!
   TBranch        *b_probe_SA_tgcHitWidth;   //!
   TBranch        *b_probe_SA_tgcHitStationNum;   //!
   TBranch        *b_probe_SA_tgcHitIsStrip;   //!
   TBranch        *b_probe_SA_tgcHitBCTag;   //!
   TBranch        *b_probe_SA_tgcHitInRoad;   //!
   TBranch        *b_probe_SA_mdtHitIsOutlier;   //!
   TBranch        *b_probe_SA_mdtHitChamber;   //!
   TBranch        *b_probe_SA_mdtHitR;   //!
   TBranch        *b_probe_SA_mdtHitZ;   //!
   TBranch        *b_probe_SA_mdtHitPhi;   //!
   TBranch        *b_probe_SA_mdtHitResidual;   //!
   TBranch        *b_probe_SA_roadAw;   //!
   TBranch        *b_probe_SA_roadBw;   //!
   TBranch        *b_probe_SA_tgcInnPhi;   //!
   TBranch        *b_probe_SA_zMin;   //!
   TBranch        *b_probe_SA_zMax;   //!
   TBranch        *b_probe_SA_rMin;   //!
   TBranch        *b_probe_SA_rMax;   //!
   TBranch        *b_probe_SA_etaMin;   //!
   TBranch        *b_probe_SA_etaMax;   //!
   TBranch        *b_probe_SA_stgcClusterZ;   //!
   TBranch        *b_probe_SA_stgcClusterR;   //!
   TBranch        *b_probe_SA_stgcClusterEta;   //!
   TBranch        *b_probe_SA_stgcClusterPhi;   //!
   TBranch        *b_probe_SA_stgcClusterResidualR;   //!
   TBranch        *b_probe_SA_stgcClusterResidualPhi;   //!
   TBranch        *b_probe_SA_stgcClusterStationEta;   //!
   TBranch        *b_probe_SA_stgcClusterStationPhi;   //!
   TBranch        *b_probe_SA_stgcClusterStationName;   //!
   TBranch        *b_probe_SA_stgcClusterType;   //!
   TBranch        *b_probe_SA_stgcClusterIsOutlier;   //!
   TBranch        *b_probe_SA_stgcClusterLayer;   //!
   TBranch        *b_probe_SA_mmClusterZ;   //!
   TBranch        *b_probe_SA_mmClusterR;   //!
   TBranch        *b_probe_SA_mmClusterEta;   //!
   TBranch        *b_probe_SA_mmClusterPhi;   //!
   TBranch        *b_probe_SA_mmClusterResidualR;   //!
   TBranch        *b_probe_SA_mmClusterResidualPhi;   //!
   TBranch        *b_probe_SA_mmClusterStationEta;   //!
   TBranch        *b_probe_SA_mmClusterStationPhi;   //!
   TBranch        *b_probe_SA_mmClusterStationName;   //!
   TBranch        *b_probe_SA_mmClusterIsOutlier;   //!
   TBranch        *b_probe_SA_mmClusterLayer;   //!
   TBranch        *b_probe_CB_pass;   //!
   TBranch        *b_probe_CB_pt;   //!
   TBranch        *b_probe_CB_eta;   //!
   TBranch        *b_probe_CB_phi;   //!
   TBranch        *b_probe_EF_pass;   //!
   TBranch        *b_probe_EF_pt;   //!
   TBranch        *b_probe_EF_eta;   //!
   TBranch        *b_probe_EF_phi;   //!

   NtupleAnalysisTool(TTree *tree=0);
   virtual ~NtupleAnalysisTool();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(HistData histData, int begin_entry, int limit_entry);
   virtual void     DrawHist(TString outputpdf, HistData histData);
   virtual int      numberOfSuperPoint();
   virtual inline float     calc_residual(float x, float y, float aw, float bw);
   virtual inline float     calc_posresidual(float x1, float y1, float x2, float y2);
   virtual inline float     calc_phiresidual(float x1, float y1);
   virtual inline void      fillByEtaPhiBin(float res, float eta, float phi, HistData histData);
   virtual inline int etaBin(float eta);
   virtual inline int phiBin(float phi);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef NtupleAnalysisTool_cxx
NtupleAnalysisTool::NtupleAnalysisTool(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/topoRoad_on_calcEffoutput.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/topoRoad_on_calcEffoutput.root");
      }
      f->GetObject("t_tap",tree);

   }
   Init(tree);
}

NtupleAnalysisTool::~NtupleAnalysisTool()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t NtupleAnalysisTool::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t NtupleAnalysisTool::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void NtupleAnalysisTool::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   trigname = 0;
   probe_L1_pass = 0;
   probe_L1_eta = 0;
   probe_L1_phi = 0;
   probe_L1_dR = 0;
   probe_L1_thrValue = 0;
   probe_L1_roiNum = 0;
   probe_L1_thrNumber = 0;
   probe_SA_pass = 0;
   probe_SA_pt = 0;
   probe_SA_eta = 0;
   probe_SA_phi = 0;
   probe_SA_etaBE = 0;
   probe_SA_phiBE = 0;
   probe_SA_etaMS = 0;
   probe_SA_phiMS = 0;
   probe_SA_tgcPt = 0;
   probe_SA_ptBarrelRadius = 0;
   probe_SA_ptBarrelSagitta = 0;
   probe_SA_ptEndcapAlpha = 0;
   probe_SA_ptEndcapBeta = 0;
   probe_SA_ptEndcapRadius = 0;
   probe_SA_ptCSC = 0;
   probe_SA_sAddress = 0;
   probe_SA_roiEta = 0;
   probe_SA_roiPhi = 0;
   probe_SA_isRpcFailure = 0;
   probe_SA_isTgcFailure = 0;
   probe_SA_superPointR_BI = 0;
   probe_SA_superPointR_BM = 0;
   probe_SA_superPointR_BO = 0;
   probe_SA_superPointR_EI = 0;
   probe_SA_superPointR_EM = 0;
   probe_SA_superPointR_EO = 0;
   probe_SA_superPointR_EE = 0;
   probe_SA_superPointR_CSC = 0;
   probe_SA_superPointR_BEE = 0;
   probe_SA_superPointR_BME = 0;
   probe_SA_superPointZ_BI = 0;
   probe_SA_superPointZ_BM = 0;
   probe_SA_superPointZ_BO = 0;
   probe_SA_superPointZ_EI = 0;
   probe_SA_superPointZ_EM = 0;
   probe_SA_superPointZ_EO = 0;
   probe_SA_superPointZ_EE = 0;
   probe_SA_superPointZ_CSC = 0;
   probe_SA_superPointZ_BEE = 0;
   probe_SA_superPointZ_BME = 0;
   probe_SA_superPointSlope_BI = 0;
   probe_SA_superPointSlope_BM = 0;
   probe_SA_superPointSlope_BO = 0;
   probe_SA_superPointSlope_EI = 0;
   probe_SA_superPointSlope_EM = 0;
   probe_SA_superPointSlope_EO = 0;
   probe_SA_superPointSlope_EE = 0;
   probe_SA_superPointSlope_CSC = 0;
   probe_SA_superPointSlope_BEE = 0;
   probe_SA_superPointSlope_BME = 0;
   probe_SA_superPointIntercept_BI = 0;
   probe_SA_superPointIntercept_BM = 0;
   probe_SA_superPointIntercept_BO = 0;
   probe_SA_superPointIntercept_EI = 0;
   probe_SA_superPointIntercept_EM = 0;
   probe_SA_superPointIntercept_EO = 0;
   probe_SA_superPointIntercept_EE = 0;
   probe_SA_superPointIntercept_CSC = 0;
   probe_SA_superPointIntercept_BEE = 0;
   probe_SA_superPointIntercept_BME = 0;
   probe_SA_superPointChi2_BI = 0;
   probe_SA_superPointChi2_BM = 0;
   probe_SA_superPointChi2_BO = 0;
   probe_SA_superPointChi2_EI = 0;
   probe_SA_superPointChi2_EM = 0;
   probe_SA_superPointChi2_EO = 0;
   probe_SA_superPointChi2_EE = 0;
   probe_SA_superPointChi2_CSC = 0;
   probe_SA_superPointChi2_BEE = 0;
   probe_SA_superPointChi2_BME = 0;
   probe_SA_rpcHitX = 0;
   probe_SA_rpcHitY = 0;
   probe_SA_rpcHitZ = 0;
   probe_SA_rpcHitR = 0;
   probe_SA_rpcHitEta = 0;
   probe_SA_rpcHitPhi = 0;
   probe_SA_rpcHitMeasPhi = 0;
   probe_SA_rpcHitLayer = 0;
   probe_SA_rpcHitStationName = 0;
   probe_SA_tgcHitZ = 0;
   probe_SA_tgcHitR = 0;
   probe_SA_tgcHitEta = 0;
   probe_SA_tgcHitPhi = 0;
   probe_SA_tgcHitWidth = 0;
   probe_SA_tgcHitStationNum = 0;
   probe_SA_tgcHitIsStrip = 0;
   probe_SA_tgcHitBCTag = 0;
   probe_SA_tgcHitInRoad = 0;
   probe_SA_mdtHitIsOutlier = 0;
   probe_SA_mdtHitChamber = 0;
   probe_SA_mdtHitR = 0;
   probe_SA_mdtHitZ = 0;
   probe_SA_mdtHitPhi = 0;
   probe_SA_mdtHitResidual = 0;
   probe_SA_roadAw = 0;
   probe_SA_roadBw = 0;
   probe_SA_tgcInnPhi = 0;
   probe_SA_zMin = 0;
   probe_SA_zMax = 0;
   probe_SA_rMin = 0;
   probe_SA_rMax = 0;
   probe_SA_etaMin = 0;
   probe_SA_etaMax = 0;
   probe_SA_stgcClusterZ = 0;
   probe_SA_stgcClusterR = 0;
   probe_SA_stgcClusterEta = 0;
   probe_SA_stgcClusterPhi = 0;
   probe_SA_stgcClusterResidualR = 0;
   probe_SA_stgcClusterResidualPhi = 0;
   probe_SA_stgcClusterStationEta = 0;
   probe_SA_stgcClusterStationPhi = 0;
   probe_SA_stgcClusterStationName = 0;
   probe_SA_stgcClusterType = 0;
   probe_SA_stgcClusterIsOutlier = 0;
   probe_SA_stgcClusterLayer = 0;
   probe_SA_mmClusterZ = 0;
   probe_SA_mmClusterR = 0;
   probe_SA_mmClusterEta = 0;
   probe_SA_mmClusterPhi = 0;
   probe_SA_mmClusterResidualR = 0;
   probe_SA_mmClusterResidualPhi = 0;
   probe_SA_mmClusterStationEta = 0;
   probe_SA_mmClusterStationPhi = 0;
   probe_SA_mmClusterStationName = 0;
   probe_SA_mmClusterIsOutlier = 0;
   probe_SA_mmClusterLayer = 0;
   probe_CB_pass = 0;
   probe_CB_pt = 0;
   probe_CB_eta = 0;
   probe_CB_phi = 0;
   probe_EF_pass = 0;
   probe_EF_pt = 0;
   probe_EF_eta = 0;
   probe_EF_phi = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("tpsumReqdRl1", &tpsumReqdRl1, &b_tpsumReqdRl1);
   fChain->SetBranchAddress("tpextdR", &tpextdR, &b_tpextdR);
   fChain->SetBranchAddress("invMass", &invMass, &b_invMass);
   fChain->SetBranchAddress("trigname", &trigname, &b_trigname);
   fChain->SetBranchAddress("n_trig", &n_trig, &b_n_trig);
   fChain->SetBranchAddress("tag_pt", &tag_pt, &b_tag_pt);
   fChain->SetBranchAddress("tag_eta", &tag_eta, &b_tag_eta);
   fChain->SetBranchAddress("tag_phi", &tag_phi, &b_tag_phi);
   fChain->SetBranchAddress("tag_extEta", &tag_extEta, &b_tag_extEta);
   fChain->SetBranchAddress("tag_extPhi", &tag_extPhi, &b_tag_extPhi);
   fChain->SetBranchAddress("tag_d0", &tag_d0, &b_tag_d0);
   fChain->SetBranchAddress("tag_z0", &tag_z0, &b_tag_z0);
   fChain->SetBranchAddress("probe_pt", &probe_pt, &b_probe_pt);
   fChain->SetBranchAddress("probe_eta", &probe_eta, &b_probe_eta);
   fChain->SetBranchAddress("probe_phi", &probe_phi, &b_probe_phi);
   fChain->SetBranchAddress("probe_extEta", &probe_extEta, &b_probe_extEta);
   fChain->SetBranchAddress("probe_extPhi", &probe_extPhi, &b_probe_extPhi);
   fChain->SetBranchAddress("probe_d0", &probe_d0, &b_probe_d0);
   fChain->SetBranchAddress("probe_z0", &probe_z0, &b_probe_z0);
   fChain->SetBranchAddress("probe_segment_n", &probe_segment_n, &b_probe_segment_n);
   fChain->SetBranchAddress("probe_segment_x", probe_segment_x, &b_probe_segment_x);
   fChain->SetBranchAddress("probe_segment_y", probe_segment_y, &b_probe_segment_y);
   fChain->SetBranchAddress("probe_segment_z", probe_segment_z, &b_probe_segment_z);
   fChain->SetBranchAddress("probe_segment_px", probe_segment_px, &b_probe_segment_px);
   fChain->SetBranchAddress("probe_segment_py", probe_segment_py, &b_probe_segment_py);
   fChain->SetBranchAddress("probe_segment_pz", probe_segment_pz, &b_probe_segment_pz);
   fChain->SetBranchAddress("probe_segment_chiSquared", probe_segment_chiSquared, &b_probe_segment_chiSquared);
   fChain->SetBranchAddress("probe_segment_numberDoF", probe_segment_numberDoF, &b_probe_segment_numberDoF);
   fChain->SetBranchAddress("probe_segment_sector", probe_segment_sector, &b_probe_segment_sector);
   fChain->SetBranchAddress("probe_segment_chamberIndex", probe_segment_chamberIndex, &b_probe_segment_chamberIndex);
   fChain->SetBranchAddress("probe_segment_etaIndex", probe_segment_etaIndex, &b_probe_segment_etaIndex);
   fChain->SetBranchAddress("probe_segment_nPrecisionHits", probe_segment_nPrecisionHits, &b_probe_segment_nPrecisionHits);
   fChain->SetBranchAddress("probe_segment_nPhiLayers", probe_segment_nPhiLayers, &b_probe_segment_nPhiLayers);
   fChain->SetBranchAddress("probe_segment_nTrigEtaLayers", probe_segment_nTrigEtaLayers, &b_probe_segment_nTrigEtaLayers);
   fChain->SetBranchAddress("probe_L1_pass", &probe_L1_pass, &b_probe_L1_pass);
   fChain->SetBranchAddress("probe_L1_eta", &probe_L1_eta, &b_probe_L1_eta);
   fChain->SetBranchAddress("probe_L1_phi", &probe_L1_phi, &b_probe_L1_phi);
   fChain->SetBranchAddress("probe_L1_dR", &probe_L1_dR, &b_probe_L1_dR);
   fChain->SetBranchAddress("probe_L1_thrValue", &probe_L1_thrValue, &b_probe_L1_thrValue);
   fChain->SetBranchAddress("probe_L1_roiNum", &probe_L1_roiNum, &b_probe_L1_roiNum);
   fChain->SetBranchAddress("probe_L1_thrNumber", &probe_L1_thrNumber, &b_probe_L1_thrNumber);
   fChain->SetBranchAddress("probe_SA_pass", &probe_SA_pass, &b_probe_SA_pass);
   fChain->SetBranchAddress("probe_SA_pt", &probe_SA_pt, &b_probe_SA_pt);
   fChain->SetBranchAddress("probe_SA_eta", &probe_SA_eta, &b_probe_SA_eta);
   fChain->SetBranchAddress("probe_SA_phi", &probe_SA_phi, &b_probe_SA_phi);
   fChain->SetBranchAddress("probe_SA_etaBE", &probe_SA_etaBE, &b_probe_SA_etaBE);
   fChain->SetBranchAddress("probe_SA_phiBE", &probe_SA_phiBE, &b_probe_SA_phiBE);
   fChain->SetBranchAddress("probe_SA_etaMS", &probe_SA_etaMS, &b_probe_SA_etaMS);
   fChain->SetBranchAddress("probe_SA_phiMS", &probe_SA_phiMS, &b_probe_SA_phiMS);
   fChain->SetBranchAddress("probe_SA_tgcPt", &probe_SA_tgcPt, &b_probe_SA_tgcPt);
   fChain->SetBranchAddress("probe_SA_ptBarrelRadius", &probe_SA_ptBarrelRadius, &b_probe_SA_ptBarrelRadius);
   fChain->SetBranchAddress("probe_SA_ptBarrelSagitta", &probe_SA_ptBarrelSagitta, &b_probe_SA_ptBarrelSagitta);
   fChain->SetBranchAddress("probe_SA_ptEndcapAlpha", &probe_SA_ptEndcapAlpha, &b_probe_SA_ptEndcapAlpha);
   fChain->SetBranchAddress("probe_SA_ptEndcapBeta", &probe_SA_ptEndcapBeta, &b_probe_SA_ptEndcapBeta);
   fChain->SetBranchAddress("probe_SA_ptEndcapRadius", &probe_SA_ptEndcapRadius, &b_probe_SA_ptEndcapRadius);
   fChain->SetBranchAddress("probe_SA_ptCSC", &probe_SA_ptCSC, &b_probe_SA_ptCSC);
   fChain->SetBranchAddress("probe_SA_sAddress", &probe_SA_sAddress, &b_probe_SA_sAddress);
   fChain->SetBranchAddress("probe_SA_roiEta", &probe_SA_roiEta, &b_probe_SA_roiEta);
   fChain->SetBranchAddress("probe_SA_roiPhi", &probe_SA_roiPhi, &b_probe_SA_roiPhi);
   fChain->SetBranchAddress("probe_SA_isRpcFailure", &probe_SA_isRpcFailure, &b_probe_SA_isRpcFailure);
   fChain->SetBranchAddress("probe_SA_isTgcFailure", &probe_SA_isTgcFailure, &b_probe_SA_isTgcFailure);
   fChain->SetBranchAddress("probe_SA_superPointR_BI", &probe_SA_superPointR_BI, &b_probe_SA_superPointR_BI);
   fChain->SetBranchAddress("probe_SA_superPointR_BM", &probe_SA_superPointR_BM, &b_probe_SA_superPointR_BM);
   fChain->SetBranchAddress("probe_SA_superPointR_BO", &probe_SA_superPointR_BO, &b_probe_SA_superPointR_BO);
   fChain->SetBranchAddress("probe_SA_superPointR_EI", &probe_SA_superPointR_EI, &b_probe_SA_superPointR_EI);
   fChain->SetBranchAddress("probe_SA_superPointR_EM", &probe_SA_superPointR_EM, &b_probe_SA_superPointR_EM);
   fChain->SetBranchAddress("probe_SA_superPointR_EO", &probe_SA_superPointR_EO, &b_probe_SA_superPointR_EO);
   fChain->SetBranchAddress("probe_SA_superPointR_EE", &probe_SA_superPointR_EE, &b_probe_SA_superPointR_EE);
   fChain->SetBranchAddress("probe_SA_superPointR_CSC", &probe_SA_superPointR_CSC, &b_probe_SA_superPointR_CSC);
   fChain->SetBranchAddress("probe_SA_superPointR_BEE", &probe_SA_superPointR_BEE, &b_probe_SA_superPointR_BEE);
   fChain->SetBranchAddress("probe_SA_superPointR_BME", &probe_SA_superPointR_BME, &b_probe_SA_superPointR_BME);
   fChain->SetBranchAddress("probe_SA_superPointZ_BI", &probe_SA_superPointZ_BI, &b_probe_SA_superPointZ_BI);
   fChain->SetBranchAddress("probe_SA_superPointZ_BM", &probe_SA_superPointZ_BM, &b_probe_SA_superPointZ_BM);
   fChain->SetBranchAddress("probe_SA_superPointZ_BO", &probe_SA_superPointZ_BO, &b_probe_SA_superPointZ_BO);
   fChain->SetBranchAddress("probe_SA_superPointZ_EI", &probe_SA_superPointZ_EI, &b_probe_SA_superPointZ_EI);
   fChain->SetBranchAddress("probe_SA_superPointZ_EM", &probe_SA_superPointZ_EM, &b_probe_SA_superPointZ_EM);
   fChain->SetBranchAddress("probe_SA_superPointZ_EO", &probe_SA_superPointZ_EO, &b_probe_SA_superPointZ_EO);
   fChain->SetBranchAddress("probe_SA_superPointZ_EE", &probe_SA_superPointZ_EE, &b_probe_SA_superPointZ_EE);
   fChain->SetBranchAddress("probe_SA_superPointZ_CSC", &probe_SA_superPointZ_CSC, &b_probe_SA_superPointZ_CSC);
   fChain->SetBranchAddress("probe_SA_superPointZ_BEE", &probe_SA_superPointZ_BEE, &b_probe_SA_superPointZ_BEE);
   fChain->SetBranchAddress("probe_SA_superPointZ_BME", &probe_SA_superPointZ_BME, &b_probe_SA_superPointZ_BME);
   fChain->SetBranchAddress("probe_SA_superPointSlope_BI", &probe_SA_superPointSlope_BI, &b_probe_SA_superPointSlope_BI);
   fChain->SetBranchAddress("probe_SA_superPointSlope_BM", &probe_SA_superPointSlope_BM, &b_probe_SA_superPointSlope_BM);
   fChain->SetBranchAddress("probe_SA_superPointSlope_BO", &probe_SA_superPointSlope_BO, &b_probe_SA_superPointSlope_BO);
   fChain->SetBranchAddress("probe_SA_superPointSlope_EI", &probe_SA_superPointSlope_EI, &b_probe_SA_superPointSlope_EI);
   fChain->SetBranchAddress("probe_SA_superPointSlope_EM", &probe_SA_superPointSlope_EM, &b_probe_SA_superPointSlope_EM);
   fChain->SetBranchAddress("probe_SA_superPointSlope_EO", &probe_SA_superPointSlope_EO, &b_probe_SA_superPointSlope_EO);
   fChain->SetBranchAddress("probe_SA_superPointSlope_EE", &probe_SA_superPointSlope_EE, &b_probe_SA_superPointSlope_EE);
   fChain->SetBranchAddress("probe_SA_superPointSlope_CSC", &probe_SA_superPointSlope_CSC, &b_probe_SA_superPointSlope_CSC);
   fChain->SetBranchAddress("probe_SA_superPointSlope_BEE", &probe_SA_superPointSlope_BEE, &b_probe_SA_superPointSlope_BEE);
   fChain->SetBranchAddress("probe_SA_superPointSlope_BME", &probe_SA_superPointSlope_BME, &b_probe_SA_superPointSlope_BME);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_BI", &probe_SA_superPointIntercept_BI, &b_probe_SA_superPointIntercept_BI);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_BM", &probe_SA_superPointIntercept_BM, &b_probe_SA_superPointIntercept_BM);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_BO", &probe_SA_superPointIntercept_BO, &b_probe_SA_superPointIntercept_BO);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_EI", &probe_SA_superPointIntercept_EI, &b_probe_SA_superPointIntercept_EI);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_EM", &probe_SA_superPointIntercept_EM, &b_probe_SA_superPointIntercept_EM);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_EO", &probe_SA_superPointIntercept_EO, &b_probe_SA_superPointIntercept_EO);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_EE", &probe_SA_superPointIntercept_EE, &b_probe_SA_superPointIntercept_EE);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_CSC", &probe_SA_superPointIntercept_CSC, &b_probe_SA_superPointIntercept_CSC);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_BEE", &probe_SA_superPointIntercept_BEE, &b_probe_SA_superPointIntercept_BEE);
   fChain->SetBranchAddress("probe_SA_superPointIntercept_BME", &probe_SA_superPointIntercept_BME, &b_probe_SA_superPointIntercept_BME);
   fChain->SetBranchAddress("probe_SA_superPointChi2_BI", &probe_SA_superPointChi2_BI, &b_probe_SA_superPointChi2_BI);
   fChain->SetBranchAddress("probe_SA_superPointChi2_BM", &probe_SA_superPointChi2_BM, &b_probe_SA_superPointChi2_BM);
   fChain->SetBranchAddress("probe_SA_superPointChi2_BO", &probe_SA_superPointChi2_BO, &b_probe_SA_superPointChi2_BO);
   fChain->SetBranchAddress("probe_SA_superPointChi2_EI", &probe_SA_superPointChi2_EI, &b_probe_SA_superPointChi2_EI);
   fChain->SetBranchAddress("probe_SA_superPointChi2_EM", &probe_SA_superPointChi2_EM, &b_probe_SA_superPointChi2_EM);
   fChain->SetBranchAddress("probe_SA_superPointChi2_EO", &probe_SA_superPointChi2_EO, &b_probe_SA_superPointChi2_EO);
   fChain->SetBranchAddress("probe_SA_superPointChi2_EE", &probe_SA_superPointChi2_EE, &b_probe_SA_superPointChi2_EE);
   fChain->SetBranchAddress("probe_SA_superPointChi2_CSC", &probe_SA_superPointChi2_CSC, &b_probe_SA_superPointChi2_CSC);
   fChain->SetBranchAddress("probe_SA_superPointChi2_BEE", &probe_SA_superPointChi2_BEE, &b_probe_SA_superPointChi2_BEE);
   fChain->SetBranchAddress("probe_SA_superPointChi2_BME", &probe_SA_superPointChi2_BME, &b_probe_SA_superPointChi2_BME);
   fChain->SetBranchAddress("probe_SA_rpcHitX", &probe_SA_rpcHitX, &b_probe_SA_rpcHitX);
   fChain->SetBranchAddress("probe_SA_rpcHitY", &probe_SA_rpcHitY, &b_probe_SA_rpcHitY);
   fChain->SetBranchAddress("probe_SA_rpcHitZ", &probe_SA_rpcHitZ, &b_probe_SA_rpcHitZ);
   fChain->SetBranchAddress("probe_SA_rpcHitR", &probe_SA_rpcHitR, &b_probe_SA_rpcHitR);
   fChain->SetBranchAddress("probe_SA_rpcHitEta", &probe_SA_rpcHitEta, &b_probe_SA_rpcHitEta);
   fChain->SetBranchAddress("probe_SA_rpcHitPhi", &probe_SA_rpcHitPhi, &b_probe_SA_rpcHitPhi);
   fChain->SetBranchAddress("probe_SA_rpcHitMeasPhi", &probe_SA_rpcHitMeasPhi, &b_probe_SA_rpcHitMeasPhi);
   fChain->SetBranchAddress("probe_SA_rpcHitLayer", &probe_SA_rpcHitLayer, &b_probe_SA_rpcHitLayer);
   fChain->SetBranchAddress("probe_SA_rpcHitStationName", &probe_SA_rpcHitStationName, &b_probe_SA_rpcHitStationName);
   fChain->SetBranchAddress("probe_SA_tgcHitZ", &probe_SA_tgcHitZ, &b_probe_SA_tgcHitZ);
   fChain->SetBranchAddress("probe_SA_tgcHitR", &probe_SA_tgcHitR, &b_probe_SA_tgcHitR);
   fChain->SetBranchAddress("probe_SA_tgcHitEta", &probe_SA_tgcHitEta, &b_probe_SA_tgcHitEta);
   fChain->SetBranchAddress("probe_SA_tgcHitPhi", &probe_SA_tgcHitPhi, &b_probe_SA_tgcHitPhi);
   fChain->SetBranchAddress("probe_SA_tgcHitWidth", &probe_SA_tgcHitWidth, &b_probe_SA_tgcHitWidth);
   fChain->SetBranchAddress("probe_SA_tgcHitStationNum", &probe_SA_tgcHitStationNum, &b_probe_SA_tgcHitStationNum);
   fChain->SetBranchAddress("probe_SA_tgcHitIsStrip", &probe_SA_tgcHitIsStrip, &b_probe_SA_tgcHitIsStrip);
   fChain->SetBranchAddress("probe_SA_tgcHitBCTag", &probe_SA_tgcHitBCTag, &b_probe_SA_tgcHitBCTag);
   fChain->SetBranchAddress("probe_SA_tgcHitInRoad", &probe_SA_tgcHitInRoad, &b_probe_SA_tgcHitInRoad);
   fChain->SetBranchAddress("probe_SA_mdtHitIsOutlier", &probe_SA_mdtHitIsOutlier, &b_probe_SA_mdtHitIsOutlier);
   fChain->SetBranchAddress("probe_SA_mdtHitChamber", &probe_SA_mdtHitChamber, &b_probe_SA_mdtHitChamber);
   fChain->SetBranchAddress("probe_SA_mdtHitR", &probe_SA_mdtHitR, &b_probe_SA_mdtHitR);
   fChain->SetBranchAddress("probe_SA_mdtHitZ", &probe_SA_mdtHitZ, &b_probe_SA_mdtHitZ);
   fChain->SetBranchAddress("probe_SA_mdtHitPhi", &probe_SA_mdtHitPhi, &b_probe_SA_mdtHitPhi);
   fChain->SetBranchAddress("probe_SA_mdtHitResidual", &probe_SA_mdtHitResidual, &b_probe_SA_mdtHitResidual);
   fChain->SetBranchAddress("probe_SA_roadAw", &probe_SA_roadAw, &b_probe_SA_roadAw);
   fChain->SetBranchAddress("probe_SA_roadBw", &probe_SA_roadBw, &b_probe_SA_roadBw);
   fChain->SetBranchAddress("probe_SA_tgcInnPhi", &probe_SA_tgcInnPhi, &b_probe_SA_tgcInnPhi);
   fChain->SetBranchAddress("probe_SA_zMin", &probe_SA_zMin, &b_probe_SA_zMin);
   fChain->SetBranchAddress("probe_SA_zMax", &probe_SA_zMax, &b_probe_SA_zMax);
   fChain->SetBranchAddress("probe_SA_rMin", &probe_SA_rMin, &b_probe_SA_rMin);
   fChain->SetBranchAddress("probe_SA_rMax", &probe_SA_rMax, &b_probe_SA_rMax);
   fChain->SetBranchAddress("probe_SA_etaMin", &probe_SA_etaMin, &b_probe_SA_etaMin);
   fChain->SetBranchAddress("probe_SA_etaMax", &probe_SA_etaMax, &b_probe_SA_etaMax);
   fChain->SetBranchAddress("probe_SA_stgcClusterZ", &probe_SA_stgcClusterZ, &b_probe_SA_stgcClusterZ);
   fChain->SetBranchAddress("probe_SA_stgcClusterR", &probe_SA_stgcClusterR, &b_probe_SA_stgcClusterR);
   fChain->SetBranchAddress("probe_SA_stgcClusterEta", &probe_SA_stgcClusterEta, &b_probe_SA_stgcClusterEta);
   fChain->SetBranchAddress("probe_SA_stgcClusterPhi", &probe_SA_stgcClusterPhi, &b_probe_SA_stgcClusterPhi);
   fChain->SetBranchAddress("probe_SA_stgcClusterResidualR", &probe_SA_stgcClusterResidualR, &b_probe_SA_stgcClusterResidualR);
   fChain->SetBranchAddress("probe_SA_stgcClusterResidualPhi", &probe_SA_stgcClusterResidualPhi, &b_probe_SA_stgcClusterResidualPhi);
   fChain->SetBranchAddress("probe_SA_stgcClusterStationEta", &probe_SA_stgcClusterStationEta, &b_probe_SA_stgcClusterStationEta);
   fChain->SetBranchAddress("probe_SA_stgcClusterStationPhi", &probe_SA_stgcClusterStationPhi, &b_probe_SA_stgcClusterStationPhi);
   fChain->SetBranchAddress("probe_SA_stgcClusterStationName", &probe_SA_stgcClusterStationName, &b_probe_SA_stgcClusterStationName);
   fChain->SetBranchAddress("probe_SA_stgcClusterType", &probe_SA_stgcClusterType, &b_probe_SA_stgcClusterType);
   fChain->SetBranchAddress("probe_SA_stgcClusterIsOutlier", &probe_SA_stgcClusterIsOutlier, &b_probe_SA_stgcClusterIsOutlier);
   fChain->SetBranchAddress("probe_SA_stgcClusterLayer", &probe_SA_stgcClusterLayer, &b_probe_SA_stgcClusterLayer);
   fChain->SetBranchAddress("probe_SA_mmClusterZ", &probe_SA_mmClusterZ, &b_probe_SA_mmClusterZ);
   fChain->SetBranchAddress("probe_SA_mmClusterR", &probe_SA_mmClusterR, &b_probe_SA_mmClusterR);
   fChain->SetBranchAddress("probe_SA_mmClusterEta", &probe_SA_mmClusterEta, &b_probe_SA_mmClusterEta);
   fChain->SetBranchAddress("probe_SA_mmClusterPhi", &probe_SA_mmClusterPhi, &b_probe_SA_mmClusterPhi);
   fChain->SetBranchAddress("probe_SA_mmClusterResidualR", &probe_SA_mmClusterResidualR, &b_probe_SA_mmClusterResidualR);
   fChain->SetBranchAddress("probe_SA_mmClusterResidualPhi", &probe_SA_mmClusterResidualPhi, &b_probe_SA_mmClusterResidualPhi);
   fChain->SetBranchAddress("probe_SA_mmClusterStationEta", &probe_SA_mmClusterStationEta, &b_probe_SA_mmClusterStationEta);
   fChain->SetBranchAddress("probe_SA_mmClusterStationPhi", &probe_SA_mmClusterStationPhi, &b_probe_SA_mmClusterStationPhi);
   fChain->SetBranchAddress("probe_SA_mmClusterStationName", &probe_SA_mmClusterStationName, &b_probe_SA_mmClusterStationName);
   fChain->SetBranchAddress("probe_SA_mmClusterIsOutlier", &probe_SA_mmClusterIsOutlier, &b_probe_SA_mmClusterIsOutlier);
   fChain->SetBranchAddress("probe_SA_mmClusterLayer", &probe_SA_mmClusterLayer, &b_probe_SA_mmClusterLayer);
   fChain->SetBranchAddress("probe_CB_pass", &probe_CB_pass, &b_probe_CB_pass);
   fChain->SetBranchAddress("probe_CB_pt", &probe_CB_pt, &b_probe_CB_pt);
   fChain->SetBranchAddress("probe_CB_eta", &probe_CB_eta, &b_probe_CB_eta);
   fChain->SetBranchAddress("probe_CB_phi", &probe_CB_phi, &b_probe_CB_phi);
   fChain->SetBranchAddress("probe_EF_pass", &probe_EF_pass, &b_probe_EF_pass);
   fChain->SetBranchAddress("probe_EF_pt", &probe_EF_pt, &b_probe_EF_pt);
   fChain->SetBranchAddress("probe_EF_eta", &probe_EF_eta, &b_probe_EF_eta);
   fChain->SetBranchAddress("probe_EF_phi", &probe_EF_phi, &b_probe_EF_phi);
   Notify();
}

Bool_t NtupleAnalysisTool::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void NtupleAnalysisTool::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
#endif // #ifdef NtupleAnalysisTool_cxx
