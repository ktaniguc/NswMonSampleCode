#ifndef HISTDATA
#define HISTDATA

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TString.h"
#include "TFile.h"

class HistData
{
  public :
    HistData(){
      HistInit();
    };
    ~HistData() {};
    void HistInit()
    {
      // for example
      for(int i=0; i<3; i++){
        m_res_mdtvsroad[i] = new TH1D(Form("m_res_mdtvsroad%d", i), "mdt vs road residual; residual; events", 250, 0, 500);
        m_res_mdtvshitroad[i] = new TH1D(Form("m_res_mdtvshitroad%d", i), "mdt vs rpc hit road residual; residual; events", 250, 0, 500);
      }
      for(int i=0; i < 4; i++){
        m_ptres_byeta[i] = new TH1D(Form("m_ptres_byeta%f", i), "p_{T} residual/#eta_{#mu}; residual; event", 40, -1, 1);
      }

      m_stgcClusterRZ   = new TH2D("m_stgcClusterRZ", "stgc clusters R-Z;Z[m];R[m];events", 80, -10, 10, 24, 0, 12);
      m_mmClusterRZ   = new TH2D("m_mmClusterRZ", "mm clusters R-Z;Z[m];R[m];events", 80, -10, 10, 24, 0, 12);
      m_stgcClusterPhi_wire   = new TH1D("m_stgcClusterPhi_wire", "stgc cluster #phi (type:wire);#phi;events", 140, -3.5, 3.5);
      m_stgcClusterR_wire   = new TH1D("m_stgcClusterR_wire", "stgc cluster R (type:wire);R[m];events", 300, 0, 6000);
      m_stgcClusterPhi_strip   = new TH1D("m_stgcClusterPhi_strip", "stgc cluster #phi (type:strip);#phi;events", 140, -3.5, 3.5);
      m_stgcClusterR_strip   = new TH1D("m_stgcClusterR_strip", "stgc cluster R (type:strip);R[m];events", 300, 0, 6000);
      m_stgcClusterResidualRPhi_pad   = new TH2D("m_stgcClusterResidualRPhi_pad", "stgc cluster ResidualR vs Phi (type:pad);|#Delta#phi|(rad);residual R[mm];events", 60, 0, 0.12, 100, -250, 250);
      m_stgcClusterResidualPhi_strip   = new TH1D("m_stgcClusterResidualPhi_strip", "stgc cluster residualPhi (type:strip);residual #phi;events", 80, -0.1, 0.3);
      m_stgcClusterResidualR_strip   = new TH1D("m_stgcClusterResidualR_strip", "stgc cluster residualR (type:strip);residual R[mm];events", 100, -250, 250);
      m_stgcClusterResidualPhi_wire   = new TH1D("m_stgcClusterResidualPhi_wire", "stgc cluster residualPhi (type:wire);|#Delta#phi|(rad);events", 60, 0, 0.12);
      m_stgcClusterResidualR_wire   = new TH1D("m_stgcClusterResidualR_wire", "stgc cluster residualR (type:wire);residual R;events", 100, 0, 1000);
      m_stgcClusterResidualR_miss_wire   = new TH1D("m_stgcClusterResidualR_miss_wire", "stgc cluster residualR (type:wire, C-side only);residual R;events", 350, 3000, 6500);
      m_mmClusterResidualR   = new TH1D("m_mmClusterResidualR", "mm cluster residualR;residual R[mm];events", 100, -250, 250);

      m_probe_extEta = new TH1D("m_probe_extEta", "probe #eta_{MS};#eta;events", 40, 1.1, 3.1);
      m_tpextdR = new TH1D("m_tpextdR", "#DeltaR_{tag and probe} at MuonSpectrometer;#DeltaR;events", 110, 0, 5.5);
      m_tpdPhi = new TH2D("m_tpdPhi", "#phi_{MS} tag vs probe;probe #phi_{MS};tag #phi_{MS};events", 120, -3, 3, 120, -3, 3);
      m_saddress = new TH1D("m_saddress", "sector address -1:endcap, >0:barrel;sector;events", 6, -2.5, 3.5);
      m_invmass = new TH1D("m_invmass", "InvMass (tag-probe);mass[GeV];events", 40, 90, 110);
      m_pt = new TH1D("m_pt", "muon pt;p_{T,#mu}[GeV];events", 150, 0, 150);
      m_pt_wcut = new TH1D("m_pt_wcut", "muon pt;p_{T,#mu}[GeV];events", 25, 0, 150);
      m_pt_l1pass = new TH1D("m_pt_l1pass", "muon pt (passed l1);p_{T,#mu}[GeV];events", 25, 0, 150);
      m_pt_sapass = new TH1D("m_pt_sapass", "muon pt (passed sa);p_{T,#mu}[GeV];events", 25, 0, 150);
      m_pt_cbpass = new TH1D("m_pt_cbpass", "muon pt (passed cb);p_{T,#mu}[GeV];events", 25, 0, 150);
      m_stgcClusterR_size = new TH1D("m_stgcClusterR_size", "The number of stgcCluster;N_{stgcCluster};events", 51, -0.5, 50.5);
      m_stgcClusterR_size_A = new TH1D("m_stgcClusterR_size_A", "The number of stgcCluster (A side);N_{stgcCluster};events", 51, -0.5, 50.5);
      m_stgcClusterR_size_C = new TH1D("m_stgcClusterR_size_C", "The number of stgcCluster (C side);N_{stgcCluster};events", 51, -0.5, 50.5);
      m_stgcIsOutlier_C = new TH1D("m_stgcClusterIsOutlier_C", "stgcCluster isOutlier (probe #eta_{MS}<0 : C-side);isOutlier;events", 6, -0.5, 5.5);
      m_stgcType_C = new TH1D("m_stgcClusterType_C", "stgcCluster Type (probe #eta_{MS}<0 : C-side);Type;events", 6, -0.5, 5.5);
      m_offsegR = new TH1D("m_offsegR", "offline segment@EndcapInner R;R[m];events", 24, 0, 12);
      m_offsegR_stgcSizeNon0 = new TH1D("m_offsegR_stgcSizeNon0", "offline segment@EndcapInner R (N_{stgcCluster} > 0);R[m];events", 24, 0, 12);
      m_offsegR_mmSizeNon0 = new TH1D("m_offsegR_mmSizeNon0", "offline segment@EndcapInner R (N_{mmCluster} > 0);R[m];events", 24, 0, 12);
      m_offseg_stgcSize0   = new TH2D("m_offseg_stgcSize0", "offline segment Z vs R (N_{stgcCluster} = 0);Z[m];R[m];events", 80, -20, 20, 24, 0, 12);
      m_offsegInnerXY_stgcSizeNon0   = new TH2D("m_offsegInnerXY_stgcSizeNon0", "offline segment X vs Y (N_{stgcCluster} > 0);X[m];Y[m];events", 110, -5.5, 5.5, 110, -5.5, 5.5);
      m_offsegInnerXY_stgcSize0   = new TH2D("m_offsegInnerXY_stgcSize0", "offline segment X vs Y (N_{stgcCluster} = 0);X[m];Y[m];events", 110, -5.5, 5.5, 110, -5.5, 5.5);
      m_offsegInnerPhi_stgcSizeNon0   = new TH1D("m_offsegInnerPhi_stgcSizeNon0", "offline segment #phi (N_{stgcCluster} > 0);#phi;events", 128, -3.2, 3.2);
      m_offsegInnerPhi_stgcSize0   = new TH1D("m_offsegInnerPhi_stgcSize0", "offline segment #phi (N_{stgcCluster} = 0);#phi;events", 128, -3.2, 3.2);
      m_offsegInnerXY_mmSizeNon0   = new TH2D("m_offsegInnerXY_mmSizeNon0", "offline segment X vs Y (N_{mmCluster} > 0);X[m];Y[m];events", 110, -5.5, 5.5, 110, -5.5, 5.5);
      m_offsegInnerXY_mmSize0   = new TH2D("m_offsegInnerXY_mmSize0", "offline segment X vs Y (N_{mmCluster} = 0);X[m];Y[m];events", 110, -5.5, 5.5, 110, -5.5, 5.5);
      m_offseg_stgcSizeNon0= new TH2D("m_offseg_stgcSizeNon0", "offline segment Z vs R (N_{stgcCluster} > 0);Z[m];R[m];events", 80, -20, 20, 24, 0, 12);
      m_mmClusterR_size = new TH1D("m_mmClusterR_size", "The number of mmClusters;N_{mmCluster};events", 51, -0.5, 50.5);
      m_offseg_mmSize0   = new TH2D("m_offseg_mmSize0", "offline segment Z vs R (N_{mmCluster} = 0);Z[m];R[m];events", 80, -20, 20, 24, 0, 12);
      m_offseg_mmSizeNon0= new TH2D("m_offseg_mmSizeNon0", "offline segment Z vs R (N_{mmCluster} > 0);Z[m];R[m];events", 80, -20, 20, 24, 0, 12);
      m_nSP = new TH1D("m_nSP", "the number of superpoints;N_{SP};events", 6, -0.5, 5.5);
      m_ratio_innSP = new TH1D("m_eff_innSP", "the events inner SP created;;events", 2, 0, 2);
      m_ratio_innSP->GetXaxis()->SetBinLabel(1, "total");
      m_ratio_innSP->GetXaxis()->SetBinLabel(2, "w/ innerSP");
      m_residual_roadvsinnSP = new TH1D("m_residual_roadvsinnSP", "residual road vs innerSP;residual[mm];events", 75, -70, 80);
      m_residual_offsegvsinnSP = new TH1D("m_residual_offsegvsinnSP", "distance between offline segment and innerSP;#Deltal[mm];events", 100, 0, 500);
      m_residual_offsegvsstgc = new TH1D("m_residual_offsegvsstgc", "residual offline segment vs stgc hit;residual[mm];events", 250, 0, 500);
      m_residual_offsegvsmm = new TH1D("m_residual_offsegvsmm", "residual offline segment vs mm hit;residual[mm];events", 250, 0, 500);
      m_offsegEIsize = new TH1D("m_offsegEIsize", "the number of offline segment@EndcapInner;N_{offline segment};events", 5, -0.5, 4.5);
      m_dtheta_SPvsOFF = new TH1D("m_dtheta_SPvsOFF", "#Delta#theta offlinesegment-SP;#Delta#theta(rad);events", 100, -0.05, 0.05);
      
      m_extEta_wcut = new TH1D("m_extEta_wcut", "#eta_{#mu} at MuonSpectrometer;#eta_{#mu} at MuonSpectrometer;events", 20, 1, 3);
      m_extEta_sp_wcut = new TH1D("m_extEta_sp_wcut", "#eta_{#mu} at MuonSpectrometer (innSP success);#eta_{#mu} at MuonSpectrometer;events", 20, 1, 3);
      m_extPhi_wcut = new TH1D("m_extPhi_wcut", "#eta_{#mu} at MuonSpectrometer;#phi_{#mu} at MuonSpectrometer;events", 35, -3.5, 3.5);
      m_extPhi_sp_wcut = new TH1D("m_extPhi_sp_wcut", "#eta_{#mu} at MuonSpectrometer (innSP success);#phi_{#mu} at MuonSpectrometer;events", 35, -3.5, 3.5);
      m_extEta_sapass_wcut = new TH1D("m_extEta_sapass_wcut", "#eta_{#mu} at MuonSpectrometer (SA pass);#eta_{#mu} at MuonSpectrometer;events", 40, 1, 3);
      m_extEta_l1pass_wcut = new TH1D("m_extEta_l1pass_wcut", "#eta_{#mu} at MuonSpectrometer (L1 pass);#eta_{#mu} at MuonSpectrometer;events", 40, 1, 3);
      m_extEta_wcut->Sumw2();
      m_extEta_sp_wcut->Sumw2();
      m_extEta_sapass_wcut->Sumw2();
      m_extEta_l1pass_wcut->Sumw2();
      m_res_OFFslopevsSTGC = new TH1D("m_res_OFFslopevsSTGC", "residual (offline slope vs STGC clusters);residual[mm];events", 100, -500, 500);
      m_res_SPvsOFFslope = new TH1D("m_res_SPvsOFFslope", "residual (offline vs SP);residual[mm];events", 120, -150, 150);
      m_res_pt = new TH1D("m_res_pt", "p_{T} residual = (1/p_{T}^{offline}-1/p_{T}^{SA})/1/p_{T}^{offline};p_{T} residual;events", 80, -3, 1);
      m_res_ptalpha = new TH1D("m_res_ptalpha", "p_{T} residual = (1/p_{T}^{offline}-1/p_{T}^{SA})/1/p_{T}^{offline};p_{T} residual;events", 80, -3, 1);
      m_res_ptbeta = new TH1D("m_res_ptbeta", "p_{T} residual = (1/p_{T}^{offline}-1/p_{T}^{SA})/1/p_{T}^{offline};p_{T} residual;events", 80, -3, 1);
    };
    void HistEnd()
    {
      // for example
      for(int i=0; i<3; i++){
        delete m_res_mdtvsroad[i];
      }
      for(int i=0; i<3; i++){
        delete m_res_mdtvshitroad[i];
      }
    };

  public : 
    // for example
    TH1D *m_res_mdtvsroad[3];
    TH1D *m_res_mdtvshitroad[3];
    TH2D *m_stgcClusterRZ;
    TH2D *m_mmClusterRZ;
    TH1D *m_stgcClusterPhi_wire;
    TH1D *m_stgcClusterR_wire;
    TH1D *m_stgcClusterPhi_strip;
    TH1D *m_stgcClusterR_strip;
    TH1D *m_stgcClusterResidualPhi_strip;
    TH2D *m_stgcClusterResidualRPhi_pad;
    TH1D *m_stgcClusterResidualR_strip;
    TH1D *m_stgcClusterResidualR_wire;
    TH1D *m_stgcClusterResidualPhi_wire;
    TH1D *m_stgcClusterResidualR_miss_wire;
    TH1D *m_mmClusterResidualR;
    TH1D *m_saddress;
    TH1D *m_invmass;
    TH1D *m_pt;
    TH1D *m_pt_wcut;
    TH1D *m_pt_l1pass;
    TH1D *m_pt_sapass;
    TH1D *m_pt_cbpass;
    TH1D *m_probe_extEta;
    TH1D *m_stgcIsOutlier_C;
    TH1D *m_stgcType_C;
    TH1D *m_tpextdR;
    TH2D *m_tpdPhi;
    TH1D *m_stgcClusterR_size;
    TH1D *m_stgcClusterR_size_A;
    TH1D *m_stgcClusterR_size_C;
    TH2D *m_offseg_stgcSize0;
    TH2D *m_offsegInnerXY_stgcSizeNon0;
    TH2D *m_offsegInnerXY_stgcSize0;
    TH2D *m_offsegInnerXY_mmSizeNon0;
    TH2D *m_offsegInnerXY_mmSize0;
    TH2D *m_offseg_stgcSizeNon0;
    TH1D *m_offsegInnerPhi_stgcSizeNon0;
    TH1D *m_offsegInnerPhi_stgcSize0;
    TH1D *m_offsegR;
    TH1D *m_offsegR_stgcSizeNon0;
    TH1D *m_mmClusterR_size;
    TH2D *m_offseg_mmSize0;
    TH2D *m_offseg_mmSizeNon0;
    TH1D *m_offsegR_mmSizeNon0;
    TH1D *m_nSP;
    TH1D *m_ratio_innSP;
    TH1D *m_residual_roadvsinnSP;
    TH1D *m_residual_offsegvsinnSP;
    TH1D *m_res_OFFslopevsSTGC;
    TH1D *m_res_SPvsOFFslope;
    TH1D *m_residual_offsegvsstgc;
    TH1D *m_residual_offsegvsmm;
    TH1D *m_offsegEIsize;
    TH1D *m_dtheta_SPvsOFF;
    TH1D *m_res_pt;
    TH1D *m_res_ptalpha;
    TH1D *m_res_ptbeta;

    //
    TH1D *m_extEta_wcut;
    TH1D *m_extEta_sp_wcut;
    TH1D *m_extPhi_wcut;
    TH1D *m_extPhi_sp_wcut;
    TH1D *m_extEta_sapass_wcut;
    TH1D *m_extEta_l1pass_wcut;
    TH1D *m_ptres_byeta[4];
};

#endif //RPCL2MUONSA_HISTDATA
