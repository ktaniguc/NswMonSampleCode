#define NtupleAnalysisTool_cxx
#include "../NtupleAnalysisTool/NtupleAnalysisTool.h"
#include <TH2.h>
#include <TStyle.h>
#include "TVector3.h"
#include "TEfficiency.h"

Int_t NtupleAnalysisTool::Cut(Long64_t entry)
{
  return 1;
}

void NtupleAnalysisTool::Loop(HistData histData, int begin_entry, int limit_entry)
{
  if (fChain == 0) return;
  Long64_t nentries;
  if(limit_entry == -1){
    nentries = fChain->GetEntries();
    begin_entry = 0;
  } else {
    nentries = limit_entry;
  }
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=begin_entry; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    std::cout << "=====> trig : " << trigname->at(m_trigchain) << std::endl;
    histData.m_invmass->Fill(invMass/1000.);
    histData.m_pt->Fill(probe_pt/1000.);

    histData.m_saddress->Fill(probe_SA_sAddress->at(m_trigchain));
    if(probe_SA_sAddress->at(m_trigchain) > -1) continue;

    int stgcSize = probe_SA_stgcClusterR->at(m_trigchain).size();
    int mmSize = probe_SA_mmClusterR->at(m_trigchain).size();
    histData.m_stgcClusterR_size->Fill(stgcSize);
    histData.m_mmClusterR_size->Fill(mmSize);
    histData.m_probe_extEta->Fill(probe_extEta);
    histData.m_tpextdR->Fill(tpextdR);
    std::vector<float> offsegR; 
    std::vector<TVector3> offseg_inner;
    std::vector<TVector3> offseg_inner_slope;
    for(int i_seg = 0; i_seg < probe_segment_n; i_seg++){
      TVector3 v3_offseg, v3_offsegP;
      v3_offseg.SetXYZ( probe_segment_x[i_seg], probe_segment_y[i_seg], probe_segment_z[i_seg] );
      v3_offsegP.SetXYZ( probe_segment_px[i_seg], probe_segment_py[i_seg], probe_segment_pz[i_seg] );
      offsegR.push_back(v3_offseg.Perp());
      if(fabs(v3_offseg.Z()) > 5000. && fabs(v3_offseg.Z()) < 8500){
        offseg_inner.push_back(v3_offseg);
        offseg_inner_slope.push_back(v3_offsegP);
      }
    }

    if(stgcSize == 0){
      for(int i_seg = 0; i_seg < probe_segment_n; i_seg++){
        histData.m_offseg_stgcSize0->Fill(probe_segment_z[i_seg]/1000, offsegR[i_seg]/1000);
      }
    } else {
      for(int i_seg = 0; i_seg < probe_segment_n; i_seg++){
        histData.m_offseg_stgcSizeNon0->Fill(probe_segment_z[i_seg]/1000, offsegR[i_seg]/1000);
      }
    }
    if(mmSize == 0){
      for(int i_seg = 0; i_seg < probe_segment_n; i_seg++){
        histData.m_offseg_mmSize0->Fill(probe_segment_z[i_seg]/1000, offsegR[i_seg]/1000);
      }
    } else {
      for(int i_seg = 0; i_seg < probe_segment_n; i_seg++){
        histData.m_offseg_mmSizeNon0->Fill(probe_segment_z[i_seg]/1000, offsegR[i_seg]/1000);
      }
    }
    if(offseg_inner.size() == 1){
      if(offseg_inner.at(0).z() > 0){
        histData.m_stgcClusterR_size_A->Fill(stgcSize);
      } else { 
        histData.m_stgcClusterR_size_C->Fill(stgcSize);
      }
      //hits efficiency
      //
      if(probe_extEta > 0){
        histData.m_offsegR->Fill(offsegR.at(0)/1000.);
        if(stgcSize > 0){ 
          histData.m_offsegR_stgcSizeNon0->Fill(offsegR.at(0)/1000.);
          histData.m_offsegInnerXY_stgcSizeNon0->Fill(offseg_inner.at(0).X()/1000. , offseg_inner.at(0).Y()/1000.);
          histData.m_offsegInnerPhi_stgcSizeNon0->Fill(offseg_inner.at(0).Phi());
        } else {
          histData.m_offsegInnerXY_stgcSize0->Fill(offseg_inner.at(0).X()/1000. , offseg_inner.at(0).Y()/1000.);
          histData.m_offsegInnerPhi_stgcSize0->Fill(offseg_inner.at(0).Phi());
        }
        if(mmSize > 0){
          histData.m_offsegR_mmSizeNon0->Fill(offsegR.at(0)/1000.);
          histData.m_offsegInnerXY_mmSizeNon0->Fill(offseg_inner.at(0).X()/1000. , offseg_inner.at(0).Y()/1000.);
        } else {
          histData.m_offsegInnerXY_mmSize0->Fill(offseg_inner.at(0).X()/1000. , offseg_inner.at(0).Y()/1000.);
        }
      } else {
        int countSTGC = 0;
        for(int i_stgc=0; i_stgc < stgcSize; i_stgc++){
          histData.m_stgcIsOutlier_C->Fill(probe_SA_stgcClusterIsOutlier->at(m_trigchain).at(i_stgc));
          histData.m_stgcType_C->Fill(probe_SA_stgcClusterType->at(m_trigchain).at(i_stgc));
          countSTGC++;
        }
        if(countSTGC > 0){
          histData.m_tpdPhi->Fill(probe_extPhi, tag_extPhi);
          for(int i_stgc = 0; i_stgc < stgcSize; i_stgc++){
            histData.m_stgcClusterResidualR_miss_wire->Fill(probe_SA_stgcClusterResidualR->at(m_trigchain).at(i_stgc));
            std::cout << "C-side L2SA stgc residualR = " << probe_SA_stgcClusterResidualR->at(m_trigchain).at(i_stgc) << std::endl;
          }
        }
      }
    }
    ////  obtain the events only having stgc, mm hits 
    //if (Cut(ientry) < 0) continue;
    if(stgcSize == 0 && mmSize == 0) continue;
    if(probe_extEta < 0) continue;
    histData.m_extEta_wcut->Fill(probe_extEta);
    histData.m_extPhi_wcut->Fill(probe_extPhi);
    histData.m_pt_wcut->Fill(probe_pt/1000.);
    int nSP = numberOfSuperPoint();
    const int EndcapInner = 3;
    histData.m_nSP->Fill(nSP);
    histData.m_ratio_innSP->Fill("total", 1);
    if (abs(probe_SA_superPointZ_EI -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EI -> at(m_trigchain) > -99990.) {
      histData.m_ratio_innSP->Fill("w/ innerSP", 1);
      histData.m_extEta_sp_wcut->Fill(probe_extEta);
      histData.m_extPhi_sp_wcut->Fill(probe_extPhi);
    }
    if(probe_L1_pass->at(m_trigchain) > 0 && probe_L1_thrValue->at(m_trigchain) >= 21000){
      histData.m_extEta_l1pass_wcut->Fill(probe_extEta);
      histData.m_pt_l1pass->Fill(probe_pt/1000.);
      if(probe_SA_pass->at(m_trigchain) > 0){
        histData.m_extEta_sapass_wcut->Fill(probe_extEta);
        histData.m_pt_sapass->Fill(probe_pt/1000.);
        if(probe_CB_pass->at(m_trigchain) > 0){
          histData.m_pt_cbpass->Fill(probe_pt/1000.);
        }
      }
    }
    std::cout << "road inner aw/bw = " << probe_SA_roadAw->at(m_trigchain).at(EndcapInner) << "/" << probe_SA_roadBw->at(m_trigchain).at(EndcapInner) << std::endl;
    if (abs(probe_SA_superPointZ_EI -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EI -> at(m_trigchain) > -99990.) {
      float residual = calc_residual(probe_SA_superPointZ_EI->at(m_trigchain), probe_SA_superPointR_EI->at(m_trigchain), probe_SA_roadAw->at(m_trigchain).at(EndcapInner), probe_SA_roadBw->at(m_trigchain).at(EndcapInner));
      histData.m_residual_roadvsinnSP->Fill(residual);
    }
    std::cout << "offline segment EndcapInner R/Z size " << offseg_inner.size() << "/" << offseg_inner.size() << std::endl;
    histData.m_offsegEIsize->Fill(offseg_inner.size());
    //stgc
    for(int i_stgc = 0; i_stgc < stgcSize; i_stgc++){
      histData.m_stgcClusterRZ->Fill(probe_SA_stgcClusterZ->at(m_trigchain).at(i_stgc)/1000., probe_SA_stgcClusterR->at(m_trigchain).at(i_stgc)/1000.);
      if(probe_SA_stgcClusterType->at(m_trigchain).at(i_stgc) == 0){ //pad
        histData.m_stgcClusterResidualRPhi_pad->Fill(probe_SA_stgcClusterResidualPhi->at(m_trigchain).at(i_stgc), probe_SA_stgcClusterResidualR->at(m_trigchain).at(i_stgc));
      }
      else if(probe_SA_stgcClusterType->at(m_trigchain).at(i_stgc) == 1){  //strip
        histData.m_stgcClusterResidualPhi_strip->Fill(probe_SA_stgcClusterResidualPhi->at(m_trigchain).at(i_stgc));
        histData.m_stgcClusterResidualR_strip->Fill(probe_SA_stgcClusterResidualR->at(m_trigchain).at(i_stgc));
        histData.m_stgcClusterPhi_strip->Fill(probe_SA_stgcClusterPhi->at(m_trigchain).at(i_stgc));
        histData.m_stgcClusterR_strip->Fill(probe_SA_stgcClusterR->at(m_trigchain).at(i_stgc));
      }
      else if(probe_SA_stgcClusterType->at(m_trigchain).at(i_stgc) == 2){  //wire
        histData.m_stgcClusterResidualR_wire->Fill(probe_SA_stgcClusterResidualR->at(m_trigchain).at(i_stgc));
//        float dPhi = calc_phiresidual(probe_SA_tgcInnPhi->at(m_trigchain), probe_SA_stgcClusterPhi->at(m_trigchain).at(i_stgc));
        histData.m_stgcClusterResidualPhi_wire->Fill(probe_SA_stgcClusterResidualPhi->at(m_trigchain).at(i_stgc));
        histData.m_stgcClusterPhi_wire->Fill(probe_SA_stgcClusterPhi->at(m_trigchain).at(i_stgc));
        histData.m_stgcClusterR_wire->Fill(fabs(probe_SA_stgcClusterR->at(m_trigchain).at(i_stgc)));
      }
    }
    for(int i_mm = 0; i_mm < mmSize; i_mm++){
      histData.m_mmClusterRZ->Fill(probe_SA_mmClusterZ->at(m_trigchain).at(i_mm)/1000., probe_SA_mmClusterR->at(m_trigchain).at(i_mm)/1000.);
      histData.m_mmClusterResidualR->Fill(probe_SA_mmClusterResidualR->at(m_trigchain).at(i_mm));
    }
    if(offseg_inner.size() == 1){
      if(probe_SA_pass->at(m_trigchain) < 0) continue;
      float posresidual = calc_posresidual(offseg_inner.at(0).Perp(), offseg_inner.at(0).z(), probe_SA_superPointR_EI->at(m_trigchain), probe_SA_superPointZ_EI->at(m_trigchain));
      histData.m_residual_offsegvsinnSP->Fill(posresidual);
      double theta_SP = atan2(probe_SA_superPointSlope_EI->at(m_trigchain), 1);
      double theta_off = atan2(offseg_inner_slope.at(0).Perp(), offseg_inner_slope.at(0).Z());
      double dtheta = theta_off - theta_SP;
      float aw_offseg = (fabs(offseg_inner_slope.at(0).Z()) > 1e-5) ? offseg_inner_slope.at(0).Perp()/offseg_inner_slope.at(0).Z() : 0;
      float bw_offseg = offseg_inner.at(0).Perp() - aw_offseg*offseg_inner.at(0).Z(); 
      float res_SPvsOFF = calc_residual(probe_SA_superPointZ_EI->at(m_trigchain), probe_SA_superPointR_EI->at(m_trigchain), aw_offseg, bw_offseg);
      std::cout << "offline pt/L2SA pt = " << probe_pt/1000. << "/" << probe_SA_pt->at(m_trigchain) << std::endl;
      std::cout << "L2SA pT by Radius/alpha/beta = " << probe_SA_ptEndcapRadius->at(m_trigchain) << "/" << probe_SA_ptEndcapAlpha->at(m_trigchain) << "/" << probe_SA_ptEndcapBeta->at(m_trigchain) << std::endl;
      float iSApt = fabs(1/probe_SA_pt->at(m_trigchain));
      float iSApt_alpha = fabs(1/probe_SA_ptEndcapAlpha->at(m_trigchain));
      float iSApt_beta = fabs(1/probe_SA_ptEndcapBeta->at(m_trigchain));
      float iofflinept = fabs(1000/probe_pt);
      float res_pt = (iofflinept - iSApt)/iofflinept;
      float res_ptalpha = (iofflinept - iSApt_alpha)/iofflinept;
      float res_ptbeta = (iofflinept - iSApt_beta)/iofflinept;
      histData.m_res_pt->Fill(res_pt);

      fillByEtaPhiBin(res_ptbeta, probe_extEta, probe_extPhi, histData);
      if(fabs(probe_SA_ptEndcapAlpha->at(m_trigchain)) > 1e-5){
        histData.m_res_ptalpha->Fill(res_ptalpha);
      }
      if(fabs(probe_SA_ptEndcapBeta->at(m_trigchain)) > 1e-5){
        histData.m_res_ptbeta->Fill(res_ptbeta);
      }
      if (abs(probe_SA_superPointZ_EI -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EI -> at(m_trigchain) > -99990.) {
        histData.m_res_SPvsOFFslope->Fill(res_SPvsOFF);
        histData.m_dtheta_SPvsOFF->Fill(dtheta);
      }
      for(int i_stgc=0; i_stgc < stgcSize; i_stgc++){
        float stgcres = calc_posresidual(offseg_inner.at(0).Perp(), offseg_inner.at(0).z(), probe_SA_stgcClusterR->at(m_trigchain).at(i_stgc), probe_SA_stgcClusterZ->at(m_trigchain).at(i_stgc));
        float res_OFFvsHITS = calc_residual(probe_SA_stgcClusterZ->at(m_trigchain).at(i_stgc), probe_SA_stgcClusterR->at(m_trigchain).at(i_stgc), aw_offseg, bw_offseg);
        histData.m_res_OFFslopevsSTGC->Fill(res_OFFvsHITS);
        histData.m_residual_offsegvsstgc->Fill(stgcres);
      }
      for(int i_mm=0; i_mm < mmSize; i_mm++){
        float mmres = calc_posresidual(offseg_inner.at(0).Perp(), offseg_inner.at(0).z(), probe_SA_mmClusterR->at(m_trigchain).at(i_mm), probe_SA_mmClusterZ->at(m_trigchain).at(i_mm));
        histData.m_residual_offsegvsmm->Fill(mmres);
      }
    }

  } // loop end
}

void NtupleAnalysisTool::DrawHist(TString pdf, HistData histData)
{
  TCanvas_opt *c1 = new TCanvas_opt();
  c1->Print( pdf + "[", "pdf");
  histData.m_saddress->Draw();
  c1->Print(pdf, "pdf");
  histData.m_probe_extEta->Draw();
  c1->Print(pdf, "pdf");
  histData.m_invmass->Draw();
  c1->Print(pdf, "pdf");
  histData.m_pt->Draw();
  c1->Print(pdf, "pdf");
  histData.m_tpextdR->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterPhi_wire->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterR_wire->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterPhi_strip->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterR_strip->Draw();
  c1->Print(pdf, "pdf");
  histData.m_mmClusterResidualR->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterResidualPhi_strip->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterResidualRPhi_pad->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterResidualPhi_wire->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterResidualR_wire->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterResidualR_strip->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterResidualR_miss_wire->Draw();
  c1->Print(pdf, "pdf");
  histData.m_tpdPhi->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_stgcIsOutlier_C->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcType_C->Draw();
  c1->Print(pdf, "pdf");
  TLegend_addfunc *leg_1 = new TLegend_addfunc(2);
  leg_1->AddSelection("Tag and probe (NoMass)");
  leg_1->AddSelection("Endcap");
  histData.m_stgcClusterR_size->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterR_size_A->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterR_size_C->Draw();
  c1->Print(pdf, "pdf");
  histData.m_stgcClusterRZ->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_mmClusterRZ->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_offseg_stgcSize0->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_offsegInnerXY_stgcSizeNon0->SetMarkerColor(kBlue);
  histData.m_offsegInnerXY_stgcSizeNon0->SetLineColor(kBlue);
  histData.m_offsegInnerXY_stgcSizeNon0->SetMarkerSize(1);
  histData.m_offsegInnerXY_stgcSizeNon0->SetMarkerStyle(4);
  histData.m_offsegInnerXY_stgcSizeNon0->Draw();
  histData.m_offsegInnerXY_stgcSize0->SetMarkerColor(kRed);
  histData.m_offsegInnerXY_stgcSize0->SetLineColor(kRed);
  histData.m_offsegInnerXY_stgcSize0->SetMarkerSize(1);
  histData.m_offsegInnerXY_stgcSize0->SetMarkerStyle(4);
  histData.m_offsegInnerXY_stgcSize0->Draw("same");
  TLegend_addfunc *leg_stgcxy = new TLegend_addfunc(1,1,2);
  leg_stgcxy->AddEntry(histData.m_offsegInnerXY_stgcSizeNon0, "N_{stgc} > 0");
  leg_stgcxy->AddEntry(histData.m_offsegInnerXY_stgcSize0, "N_{stgc} = 0");
  leg_stgcxy->Draw("same");
  c1->Print(pdf, "pdf");
  histData.m_offsegInnerPhi_stgcSizeNon0->SetMarkerColor(kBlue);
  histData.m_offsegInnerPhi_stgcSizeNon0->SetLineColor(kBlue);
  histData.m_offsegInnerPhi_stgcSize0->SetMarkerColor(kRed);
  histData.m_offsegInnerPhi_stgcSize0->SetLineColor(kRed);
  histData.m_offsegInnerPhi_stgcSize0->Draw();
  histData.m_offsegInnerPhi_stgcSizeNon0->Draw("hist same");
  leg_stgcxy->Draw("same");
  c1->Print(pdf, "pdf");
  histData.m_offsegInnerXY_mmSizeNon0->SetMarkerColor(kBlue);
  histData.m_offsegInnerXY_mmSizeNon0->SetLineColor(kBlue);
  histData.m_offsegInnerXY_mmSizeNon0->SetMarkerSize(1);
  histData.m_offsegInnerXY_mmSizeNon0->SetMarkerStyle(4);
  histData.m_offsegInnerXY_mmSizeNon0->Draw();
  histData.m_offsegInnerXY_mmSize0->SetMarkerColor(kRed);
  histData.m_offsegInnerXY_mmSize0->SetLineColor(kRed);
  histData.m_offsegInnerXY_mmSize0->SetMarkerSize(1);
  histData.m_offsegInnerXY_mmSize0->SetMarkerStyle(4);
  histData.m_offsegInnerXY_mmSize0->Draw("same");
  TLegend_addfunc *leg_mmxy = new TLegend_addfunc(1,1,2);
  leg_mmxy->AddEntry(histData.m_offsegInnerXY_mmSizeNon0, "N_{mm} > 0");
  leg_mmxy->AddEntry(histData.m_offsegInnerXY_mmSize0, "N_{mm} = 0");
  leg_mmxy->Draw("same");
  c1->Print(pdf, "pdf");
  histData.m_offseg_stgcSizeNon0->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_offsegR->Draw();
  c1->Print(pdf, "pdf");
  histData.m_offsegR_stgcSizeNon0->Draw();
  c1->Print(pdf, "pdf");
  histData.m_offsegR_mmSizeNon0->Draw();
  c1->Print(pdf, "pdf");
  TEfficiency *m_eff_stgcHits_dR  = new TEfficiency(*(histData.m_offsegR_stgcSizeNon0), *(histData.m_offsegR));
  m_eff_stgcHits_dR->SetTitle("[N_{stgcClusters} > 0 muon events]/[all muon events];offline segment R[m];#epsilon");
  m_eff_stgcHits_dR->Draw();
  c1->Print(pdf, "pdf");
  histData.m_mmClusterR_size->Draw();
  c1->Print(pdf, "pdf");
  histData.m_offseg_mmSize0->Draw("colZ");
  c1->Print(pdf, "pdf");
  histData.m_offseg_mmSizeNon0->Draw("colZ");
  c1->Print(pdf, "pdf");
  TEfficiency *m_eff_mmHits_dR  = new TEfficiency(*(histData.m_offsegR_mmSizeNon0), *(histData.m_offsegR));
  m_eff_mmHits_dR->SetTitle("[N_{mmClusters} > 0 muon events]/[all muon events];offline segment R[m];#epsilon");
  m_eff_mmHits_dR->Draw();
  c1->Print(pdf, "pdf");
  histData.m_nSP->Draw();
  c1->Print(pdf, "pdf");
  histData.m_offsegEIsize->Draw();
  c1->Print(pdf, "pdf");
  histData.m_ratio_innSP->Draw("hist text");
  c1->Print(pdf, "pdf");
  histData.m_residual_roadvsinnSP->Draw();
  c1->Print(pdf, "pdf");
  histData.m_residual_offsegvsinnSP->Draw();
  c1->Print(pdf, "pdf");
  histData.m_dtheta_SPvsOFF->Draw();
  c1->Print(pdf, "pdf");
  histData.m_res_OFFslopevsSTGC->Draw();
  c1->Print(pdf, "pdf");
  histData.m_res_SPvsOFFslope->Draw();
  c1->Print(pdf, "pdf");
  histData.m_res_pt->Draw();
  c1->Print(pdf, "pdf");
  histData.m_res_ptalpha->SetMarkerColor(kRed);
  histData.m_res_ptbeta->SetMarkerColor(kBlue);
  histData.m_res_ptalpha->SetLineColor(kRed);
  histData.m_res_ptbeta->SetLineColor(kBlue);
  histData.m_res_ptalpha->Draw();
  histData.m_res_ptbeta->Draw("same");
  TLegend_addfunc* leg_pt = new TLegend_addfunc(0,0,2);
  leg_pt->AddEntry(histData.m_res_ptalpha, "#alpha");
  leg_pt->AddEntry(histData.m_res_ptbeta, "#beta");
  leg_pt->Draw("same");
  c1->Print(pdf, "pdf");
  histData.m_residual_offsegvsstgc->Draw();
  c1->Print(pdf, "pdf");
  histData.m_residual_offsegvsmm->Draw();
  c1->Print(pdf, "pdf");
  TLegend_addfunc* leg_byeta = new TLegend_addfunc(0,0,4);
  leg_byeta->AddEntry(histData.m_ptres_byeta[0], "1.3#leq#eta<1.6");
  histData.m_ptres_byeta[0]->Draw();
  histData.m_ptres_byeta[0]->GetYaxis()->SetRangeUser(0, 50);
  for(int i=1; i<4; i++){
    histData.m_ptres_byeta[i]->SetLineColor(i+1);
    histData.m_ptres_byeta[i]->SetMarkerColor(i+1);
    leg_byeta->AddEntry(histData.m_ptres_byeta[i], Form("%3.1f#leq#eta<%3.1f", 1.3+0.3*i, 1.6+0.3*i));
    histData.m_ptres_byeta[i]->Draw("same");
  }
  leg_byeta->Draw("same");
  c1->Print(pdf, "pdf");
  c1->Print( pdf + "]", "pdf");
  delete c1;
}

int NtupleAnalysisTool::numberOfSuperPoint()
{
  int number = 0;
  if (abs(probe_SA_superPointZ_BI -> at(m_trigchain)) > 0. && probe_SA_superPointZ_BI -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_BM -> at(m_trigchain)) > 0. && probe_SA_superPointZ_BM -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_BO -> at(m_trigchain)) > 0. && probe_SA_superPointZ_BO -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_EI -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EI -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_EM -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EM -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_EO -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EO -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_EE -> at(m_trigchain)) > 0. && probe_SA_superPointZ_EE -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_CSC -> at(m_trigchain)) > 0. && probe_SA_superPointZ_CSC -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_BEE -> at(m_trigchain)) > 0. && probe_SA_superPointZ_BEE -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  if (abs(probe_SA_superPointZ_BME -> at(m_trigchain)) > 0. && probe_SA_superPointZ_BME -> at(m_trigchain) > -99990.) {
    number += 1;
  }
  return number;
}

inline float NtupleAnalysisTool::calc_residual(float x, float y, float aw, float bw)
{
  const float ZERO_LIMIT = 1e-4;
  if( fabs(aw) < ZERO_LIMIT ) return y-bw;
  float ia  = 1/aw;
  float iaq = ia*ia;
  float dz  = x - (y-bw)*ia;
  return dz/sqrt(1.+iaq);
}

inline float NtupleAnalysisTool::calc_posresidual(float x1, float y1, float x2, float y2)
{
  float dx = x1-x2;
  float dy = y1-y2;
  float dxq = dx*dx;
  float dyq = dy*dy;
  return sqrt(dxq+dyq);
}

inline float NtupleAnalysisTool::calc_phiresidual(float x1, float y1)
{
  float dphi = fabs(x1 - y1);
  if(dphi > 0){
    if( dphi > M_PI*2 ) dphi = dphi - M_PI*2;
    if( dphi > M_PI ) dphi = M_PI*2 - dphi;
  } else {
    if( fabs(dphi) > M_PI*2 ) dphi = dphi + M_PI*2;
    if( fabs(dphi) > M_PI ) dphi = -M_PI*2 + dphi;
  }
  return dphi;
}

inline void NtupleAnalysisTool::fillByEtaPhiBin(float res, float eta, float phi, HistData histData)
{
  int index = etaBin(eta);
  if(index > -1) histData.m_ptres_byeta[index]->Fill(res);
}

inline int NtupleAnalysisTool::etaBin(float eta)
{
  if(1.3 <= eta && eta < 1.6) return 0;
  else if(1.6 <= eta && eta < 1.9) return 1;
  else if(1.9 <= eta && eta < 2.2) return 2;
  else if(2.2 <= eta && eta < 2.5) return 3;
  else return -1;
}

inline int NtupleAnalysisTool::phiBin(float phi)
{
  if(-2 <= phi && phi < -0.6) return 0;
  else if(-0.4 <= phi && phi < 1) return 1;
  else if(1 <= phi && phi < 2.4) return 2;
  else if(2.4 <= phi || phi < -2.2) return 3;
  else return -1;
}
